package co.technical.test.inspiro.vendingmachine.constants;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;

public class Format {
    public static NumberFormat formatPrice = new DecimalFormat("#0.00");
    public static SimpleDateFormat formatterDateTimeLogMachine= new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss ");
}
