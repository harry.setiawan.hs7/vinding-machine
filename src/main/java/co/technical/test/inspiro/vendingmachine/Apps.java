package co.technical.test.inspiro.vendingmachine;

import co.technical.test.inspiro.vendingmachine.console.impl.ConsoleImpl;
import co.technical.test.inspiro.vendingmachine.console.impl.ContentReaderImpl;
import co.technical.test.inspiro.vendingmachine.model.MenuItem;
import co.technical.test.inspiro.vendingmachine.model.MoneyItem;

import java.util.ArrayList;
import java.util.Scanner;

public class Apps {
    public ArrayList<MoneyItem> moneyItems  = new ArrayList<>();
    public ArrayList<MenuItem> menuItems    = new ArrayList<>();
    public ContentReaderImpl contentReader  = null;
    public ConsoleImpl console              = null;

    public Apps() throws Exception{
        contentReader   = new ContentReaderImpl(this);
        console         = new ConsoleImpl(this);

        moneyItems.addAll(contentReader.readerMoneyContent());
        menuItems.addAll(contentReader.readerMenuContent());
        console.build();
    }

    public static void main(String[] args) throws Exception{
        new Apps();
    }
    
}






