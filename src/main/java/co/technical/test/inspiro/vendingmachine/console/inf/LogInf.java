package co.technical.test.inspiro.vendingmachine.console.inf;

public interface LogInf {
    void writeLogMachine(String content) throws Exception;
}
