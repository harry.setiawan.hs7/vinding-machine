package co.technical.test.inspiro.vendingmachine.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Utils {

    public static boolean checkDigit(String content){
        String regexDigit = "[0-9]+";
        return content.matches(regexDigit);
    }

    public static String formatRupiah(Double content){
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("");
        formatRp.setMonetaryDecimalSeparator('.');
        formatRp.setGroupingSeparator(',');

        kursIndonesia.setDecimalFormatSymbols(formatRp);
        return kursIndonesia.format(content).replaceAll("\\.00","");
    }
}
