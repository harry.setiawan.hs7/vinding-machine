package co.technical.test.inspiro.vendingmachine.console.inf;

import co.technical.test.inspiro.vendingmachine.model.MenuItem;
import co.technical.test.inspiro.vendingmachine.model.MoneyItem;

import java.util.ArrayList;

public interface ContentReaderInf {
     ArrayList<MoneyItem> readerMoneyContent() throws Exception;
     ArrayList<MenuItem> readerMenuContent() throws Exception;
}