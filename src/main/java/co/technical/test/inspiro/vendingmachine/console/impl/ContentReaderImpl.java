package co.technical.test.inspiro.vendingmachine.console.impl;

import co.technical.test.inspiro.vendingmachine.Apps;
import co.technical.test.inspiro.vendingmachine.console.inf.ContentReaderInf;
import co.technical.test.inspiro.vendingmachine.console.inf.LogInf;
import co.technical.test.inspiro.vendingmachine.constants.Files;
import co.technical.test.inspiro.vendingmachine.constants.Format;
import co.technical.test.inspiro.vendingmachine.model.MenuItem;
import co.technical.test.inspiro.vendingmachine.model.MoneyItem;
import co.technical.test.inspiro.vendingmachine.utils.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ContentReaderImpl implements ContentReaderInf, LogInf {

    Apps app = null;

    public ContentReaderImpl(Apps app){
        this.app = app;
    }

    @Override
    public ArrayList<MoneyItem> readerMoneyContent() throws Exception{
        ArrayList<MoneyItem> moneyItems = new ArrayList<>();
        File file=new File(Files.moneyDir);
        FileReader fr=new FileReader(file);
        BufferedReader br=new BufferedReader(fr);
        StringBuffer sb=new StringBuffer();
        String line;
        while((line=br.readLine())!=null){
            moneyItems.add(parseMoneyContent(line));
        }
        fr.close();
        return moneyItems;
    }

    private MoneyItem parseMoneyContent(String content) throws Exception{
        MoneyItem item = new MoneyItem();
        String[] dataContents = content.split("::");
        for(int i = 0; i < dataContents.length ; i++){
            if(i == 0){
                if(Utils.checkDigit(dataContents[i])) {
                    item.setId(Integer.parseInt(dataContents[i]));
                }else{
                    throw new Exception("Ooppss..!! Format id uang tidak sesuai.");
                }
            }
            if(i == 1){
                if(Utils.checkDigit(dataContents[i])) {
                    item.setMoney(Double.parseDouble(dataContents[i]));
                }else{
                    throw new Exception("Ooppss..!! Format uang tidak sesuai.");
                }
            }
        }
        return item;
    }

    @Override
    public ArrayList<MenuItem> readerMenuContent() throws Exception{
        ArrayList<MenuItem> moneyItems = new ArrayList<>();
        File file=new File(Files.menuDir);
        FileReader fr= new FileReader(file);
        BufferedReader br=new BufferedReader(fr);
        StringBuffer sb=new StringBuffer();
        String line;
        while((line=br.readLine())!=null){
            moneyItems.add(parseMenuContent(line));
        }
        fr.close();
        return moneyItems;
    }


    private MenuItem parseMenuContent(String content) throws Exception{
        MenuItem item = new MenuItem();
        String[] dataContents = content.split("::");
        for(int i = 0; i < dataContents.length ; i++){
            if(i == 0){
                if(Utils.checkDigit(dataContents[i])) {
                    item.setId(Integer.parseInt(dataContents[i]));
                }else{
                    throw new Exception("Ooppss..!! Format id menu tidak sesuai.");
                }
            }
            if(i == 1){
                item.setName(dataContents[i]);
            }
            if(i == 2){
                if(Utils.checkDigit(dataContents[i])) {
                    item.setPrice(Double.parseDouble(dataContents[i]));
                }else{
                    throw new Exception("Ooppss..!! Format harga menu tidak sesuai.");
                }
            }
            if(i == 3){
                if(Utils.checkDigit(dataContents[i])) {
                    item.setStock(Integer.parseInt(dataContents[i]));
                }else{
                    throw new Exception("Ooppss..!! Format stok menu tidak sesuai.");
                }
            }
        }
        return item;
    }

    @Override
    public void writeLogMachine(String content) throws Exception{
        FileWriter fw = new FileWriter(Files.logDir,true);
        Calendar calendar = Calendar.getInstance();
        fw.write("[ " + Format.formatterDateTimeLogMachine.format(calendar.getTime()) +"] :");
        fw.write(content + "\n");
        fw.close();
    }

    public void rewriteDataMenu() throws Exception{
        FileWriter fw = new FileWriter(Files.menuDir,false);
        for(MenuItem item : app.menuItems) {
            fw.write(""
                    + item.getId() + "::"
                    + item.getName() + "::"
                    + item.getPrice().intValue() + "::"
                    + item.getStock() + "::"
                    + "\n");
        }
        fw.close();
    }
}
