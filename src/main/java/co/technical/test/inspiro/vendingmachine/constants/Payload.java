package co.technical.test.inspiro.vendingmachine.constants;

public class Payload {
    public final static String titleMoney               = "Uang Pecahan yang diterima";
    public final static String titleMenu                = "Menu yang tersedia";
    public final static String titleValue               = "Jumlah value yang diinginkan";
    public final static String titleRp                  = "Rp.";
    public final static String titleNotAvailable        = "Tidak Tersedia/Habis";

    public final static String moneySelector            = "Pilih pecahan pata uang Anda : ";
    public final static String menuSelector             = "Pilih menu yang Anda inginkan : ";
    public final static String valueSelector            = "Masukan jumlah yang Anda inginkan : ";


    public final static String errorTriggerNotFound     = "Data yang diinput tidak terdaftar dalam data.";
    public final static String errorTriggerMoneyNotPass = "Maaf uang Anda tidak mencukupi.";
    public final static String errorStockNotAvailable   = "Maaf untuk menu #NAME# saat ini tidak tersedia.";

    public final static String line                     = "==============================================================";
    public final static String exit                     = "(0)   Exit";

    public final static String descMachineON            = "Vending Machine START";
    public final static String descMachineOFF           = "Vending Machine STOP";
    public final static String descInputMoney           = "Uang Rp. #NOMINAL# dimasukan.";
    public final static String descInputMenu            = "Menu  #NAME# dipesan.";
    public final static String descInputValue           = "Value pesanan  berjumlah #VALUE# .";
    public final static String descCancelMoney          = "Uang Rp. #NOMINAL# dikembalikan.";
    public final static String descTotal                = "Total Harga Rp. #NOMINAL#.";
    public final static String descCachback             = "Total Uang Kembalian Rp. #NOMINAL#.";
    public final static String descCancelMenu           = "Pesanan menu #NAME# dibatalkan.";
    public final static String descStockMenu            = "Sisa stok menu #NAME# berjumlah #COUNT#.";
}
