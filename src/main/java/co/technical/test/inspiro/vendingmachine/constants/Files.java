package co.technical.test.inspiro.vendingmachine.constants;

public class Files {
    private final static String rootDir=  System.getProperty("user.dir");
    public final static String moneyDir = rootDir + "/src/main/resources/mata-uang.dat";
    public final static String menuDir = rootDir + "/src/main/resources/menu.dat";
    public final static String logDir = rootDir + "/src/main/resources/log.dat";
}
