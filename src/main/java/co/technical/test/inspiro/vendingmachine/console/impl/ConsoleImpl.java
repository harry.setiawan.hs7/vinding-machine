package co.technical.test.inspiro.vendingmachine.console.impl;

import co.technical.test.inspiro.vendingmachine.Apps;
import co.technical.test.inspiro.vendingmachine.console.inf.ConsoleInf;
import co.technical.test.inspiro.vendingmachine.console.inf.LogInf;
import co.technical.test.inspiro.vendingmachine.constants.Files;
import co.technical.test.inspiro.vendingmachine.constants.Format;
import co.technical.test.inspiro.vendingmachine.constants.Payload;
import co.technical.test.inspiro.vendingmachine.model.MenuItem;
import co.technical.test.inspiro.vendingmachine.model.MoneyItem;
import co.technical.test.inspiro.vendingmachine.utils.Utils;
import sun.misc.IOUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Scanner;

public class ConsoleImpl implements ConsoleInf {
    Apps app;
    int inputMoneySelected, inputMenuSelected, inputMenuValue;
    Scanner inputConsole = new Scanner(System.in);

    public ConsoleImpl(Apps app) {
        this.app = app;
    }

    private void trigerMoneySelection() throws Exception{
        System.out.print(Payload.moneySelector);
        String input = String.valueOf(inputConsole.nextLine());
        if(Utils.checkDigit(input)){
            if(Integer.parseInt(input) == 0){
                exit(3);
            }
            if(Integer.parseInt(input) <= app.moneyItems.size()){
                inputMoneySelected = Integer.parseInt(input) - 1;
                app.contentReader.writeLogMachine(Payload.descInputMoney
                        .replaceAll("#NOMINAL#", Utils.formatRupiah(app.moneyItems.get(inputMoneySelected).getMoney())
                        ));
            }else{
                displayError(Payload.errorTriggerNotFound);
                trigerMoneySelection();
            }
        }else{
            displayError(Payload.errorTriggerNotFound);
            trigerMoneySelection();
        }
    }

    private void trigerMenuSelection() throws Exception {
        System.out.print(Payload.menuSelector);
        String input = String.valueOf(inputConsole.nextLine());
        if(Utils.checkDigit(input)){
            if(Integer.parseInt(input) == 0){
                exit(2);
            }
            if(Integer.parseInt(input) <= app.menuItems.size()){
                inputMenuSelected = Integer.parseInt(input) - 1;
                if(app.menuItems.get(inputMenuSelected).getStock() > 0) {
                    app.contentReader.writeLogMachine(Payload.descInputMenu
                            .replaceAll("#NAME#", app.menuItems.get(inputMenuSelected).getName()
                            ));
                }else{
                    displayError(Payload.errorStockNotAvailable
                            .replaceAll("#NAME#", app.menuItems.get(inputMenuSelected).getName().toUpperCase(Locale.ROOT))
                    );
                    trigerMenuSelection();
                }
            }else{
                displayError(Payload.errorTriggerNotFound);
                trigerMenuSelection();
            }
        }else{
            displayError(Payload.errorTriggerNotFound);
            trigerMenuSelection();
        }
    }

    private void trigerValueSelection() throws Exception {
        System.out.print(Payload.valueSelector);
        String input = String.valueOf(inputConsole.nextLine());
        if(Utils.checkDigit(input)){
            if(Integer.parseInt(input) == 0){
                exit(3);
            }else{
                inputMenuValue = Integer.parseInt(input);
                if(Integer.parseInt(input) <= app.menuItems.get(inputMenuSelected).getStock()){
                    Double totalPrice = inputMenuValue * app.menuItems.get(inputMenuSelected).getPrice();
                    if(totalPrice <= app.moneyItems.get(inputMoneySelected).getMoney()) {
                        Double cashback = app.moneyItems.get(inputMoneySelected).getMoney() - totalPrice;
                        app.menuItems.get(inputMenuSelected).setStock(
                               app.menuItems.get(inputMenuSelected).getStock() - inputMenuValue
                        );

                        app.contentReader.writeLogMachine(Payload.descInputValue
                                .replaceAll("#VALUE#", String.valueOf(inputMenuValue))
                        );

                        app.contentReader.writeLogMachine(Payload.descTotal
                                .replaceAll("#NOMINAL#", String.valueOf(Utils.formatRupiah(totalPrice))))
                        ;

                        app.contentReader.writeLogMachine(Payload.descCachback
                                .replaceAll("#NOMINAL#", String.valueOf(Utils.formatRupiah(cashback)))
                        );

                        app.contentReader.writeLogMachine(Payload.descStockMenu
                                .replaceAll("#NAME#", app.menuItems.get(inputMenuSelected).getName())
                                .replaceAll("#COUNT#", String.valueOf(app.menuItems.get(inputMenuSelected).getStock()))
                        );

                        System.out.println(Payload.descInputValue
                                .replaceAll("#VALUE#", String.valueOf(inputMenuValue)));

                        System.out.println(Payload.descTotal
                                .replaceAll("#NOMINAL#", String.valueOf(Utils.formatRupiah(totalPrice)))
                        );

                        System.out.println(Payload.descCachback
                                .replaceAll("#NOMINAL#", String.valueOf(Utils.formatRupiah(cashback)))
                        );

                        System.out.println(Payload.descStockMenu
                                .replaceAll("#NAME#", app.menuItems.get(inputMenuSelected).getName())
                                .replaceAll("#COUNT#", String.valueOf(app.menuItems.get(inputMenuSelected).getStock()))
                        );

                    }else{
                        displayError(Payload.errorTriggerMoneyNotPass);
                        trigerValueSelection();
                    }
                }else{

                }
            }
        }else{
            displayError(Payload.errorTriggerNotFound);
            trigerValueSelection();
        }
    }

    private void displayConsoleMoney() throws Exception {
        app.contentReader.writeLogMachine(Payload.descMachineON);
        displayHeader(Payload.titleMoney);
        for(MoneyItem item : app.moneyItems){
            String id    = "(" + String.valueOf(item.getId())  + ") ";
            String money = Utils.formatRupiah(item.getMoney());

            if(id.length() < 6){
                for(int n = id.length(); n < 6; n++){
                    id = id + " ";
                }
            }
            if(money.length() < 7){
                for(int n = money.length(); n < 7; n++){
                    money = " " + money;
                }
            }
            System.out.println(id + Payload.titleRp + money);
        }
        System.out.println(Payload.exit);
    }

    private void displayConsoleMenu() {
        displayHeader(Payload.titleMenu);
        for(MenuItem item : app.menuItems){
            String id    = "(" + String.valueOf(item.getId())  + ") ";
            String name  = item.getName().toUpperCase(Locale.ROOT);
            String price = Utils.formatRupiah(item.getPrice());
            String stock = String.valueOf(item.getStock());

            if(id.length() < 6){
                for(int n = id.length(); n < 6; n++){
                    id = id + " ";
                }
            }
            if(name.length() < 10){
                for(int n = name.length(); n < 10; n++){
                    name = name + " ";
                }
            }
            if(price.length() < 7){
                for(int n = price.length(); n < 7; n++){
                    price = " " + price;
                }
            }
            if(item.getStock() < 1){
                stock = Payload.titleNotAvailable;
            }
            System.out.println(id + name + "Rp. " + price + "  [" + stock + "]");
        }
        System.out.println(Payload.exit);
    }

    private void displayConsoleValue() {
        displayHeader(Payload.titleValue);
        System.out.println(Payload.exit);
    }

    private void displayHeader(String header) {
        System.out.println();
        System.out.println(Payload.line);
        System.out.println(header);
        System.out.println(Payload.line);
    }

    private void displayError(String content) {
        System.out.println();
        System.out.println(content);
    }

    private void exit(int action) throws Exception {
        System.out.println();
        switch (action){
            case 2:
                System.out.println(Payload.descCancelMoney
                        .replaceAll("#NOMINAL#", Utils.formatRupiah(app.moneyItems.get(inputMoneySelected).getMoney())
                        ));
                app.contentReader.writeLogMachine(Payload.descCancelMoney
                        .replaceAll("#NOMINAL#", Utils.formatRupiah(app.moneyItems.get(inputMoneySelected).getMoney())
                        ));
                break;
            case 3:
                System.out.println(Payload.descCancelMenu
                        .replaceAll("#NAME#", app.menuItems.get(inputMenuSelected).getName()
                        ));
                System.out.println(Payload.descCancelMoney
                        .replaceAll("#NOMINAL#", Utils.formatRupiah(app.moneyItems.get(inputMoneySelected).getMoney())
                        ));
                app.contentReader.writeLogMachine(Payload.descCancelMenu
                        .replaceAll("#NAME#", app.menuItems.get(inputMenuSelected).getName()
                        ));
                app.contentReader.writeLogMachine(Payload.descCancelMoney
                        .replaceAll("#NOMINAL#", Utils.formatRupiah(app.moneyItems.get(inputMoneySelected).getMoney())
                        ));
                break;
        }
        app.contentReader.writeLogMachine(Payload.descMachineOFF);
        System.exit(0);
    }

    @Override
    public void build() throws Exception {
        displayConsoleMoney();
        trigerMoneySelection();
        displayConsoleMenu();
        trigerMenuSelection();
        displayConsoleValue();
        trigerValueSelection();
        app.contentReader.rewriteDataMenu();
        app = new Apps();
    }
}
