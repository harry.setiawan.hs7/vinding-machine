package co.technical.test.inspiro.vendingmachine.console.inf;

import co.technical.test.inspiro.vendingmachine.Apps;
import co.technical.test.inspiro.vendingmachine.model.MenuItem;
import co.technical.test.inspiro.vendingmachine.model.MoneyItem;

import java.util.ArrayList;

public interface ConsoleInf {
     void build() throws Exception;
}